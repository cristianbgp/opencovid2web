import React from "react";

const VaccinationMap = () => {
  return (
    <section className="w-100 section-vaccination-map">
      <div className="jafeth-filters">
        {/* <FilterRegion text="Busca una región" theme="container"/>             */}
      </div>
      {/* <Map
                            style="mapbox://styles/mapbox/streets-v9"
                            containerStyle={{
                                height: '500px',
                                width: '100%',
                            }}
                            center={[-75.4563193462673,-9.73541086388338]}
                            zoom={[4]}
                        >
                            
                            <Marker coordinates={[-75.4563193462673,-9.73541086388338]}>
                                    <div className="container bg-white">You are here</div>
                            </Marker>
                            
                    </Map> */}
      <iframe
        id="inlineFrameExample"
        width="100%"
        frameBorder="0"
        src="https://gis.minsa.gob.pe/gisvacunas"
        className="mapa_s"
      ></iframe>
    </section>
  );
};

export default VaccinationMap;
